class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@umai.com'
  layout 'mailer'
end
