class ReservationMailer < ApplicationMailer

  def guest_email reservation_history
    @reservation_last = reservation_history
    reservation = reservation_history.reservation
    @guest = reservation.guest
    @reservation_number = "#{reservation.restaurant.code}-#{reservation.id.to_s}"
    if reservation.reservation_histories.count > 1
      @reservation_previous = reservation.reservation_histories.order(:id).last(2).first
    end

    subject = "Reservation Details for #{reservation.restaurant.code}-#{reservation.id.to_s}"
    mail(
      :to => reservation.guest.email,
      :subject => subject
    )

  end

  def restaurant_email reservation_history
    @reservation_last = reservation_history
    reservation = reservation_history.reservation
    @reservation_number = "#{reservation.restaurant.code}-#{reservation.id.to_s}"
    reservation_type = 'New'
    if reservation.reservation_histories.count > 1
      reservation_type = "Updated"
    end
    subject = "#{reservation_type} Reservation Details for #{reservation.restaurant.code}-#{reservation.id.to_s}"
    mail(
      :to => reservation.guest.email,
      :subject => subject
    )
  end
end
