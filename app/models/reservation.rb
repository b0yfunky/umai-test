class Reservation < ApplicationRecord
  include Formats::Reservation
  belongs_to :restaurant
  belongs_to :guest
  belongs_to :restaurant_table
  belongs_to :restaurant_shift
  has_many :reservation_histories

  after_create :create_reservation_history

  after_commit :create_reservation_history, on: :update

  def create_reservation_history
    ReservationHistory.create!(reservation: self, reservation_time: self.reservation_time, reservation_date: self.reservation_date, restaurant_table_id: self.restaurant_table_id, restaurant_shift_id: self.restaurant_shift_id, no_of_pax: self.no_of_pax)
  end
end
