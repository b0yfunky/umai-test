class Restaurant < ApplicationRecord
  has_many :restaurant_tables
  has_many :restaurant_shifts
  has_many :reservations

  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
  validates :name, presence: true
  validates :phone, presence: true, format: { with: /\A\d+\z/, message: 'The entry can only contain numbers' }


end
