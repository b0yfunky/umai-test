class RestaurantTable < ApplicationRecord
  belongs_to :restaurant
  before_validation :ensure_maximum_count

  def can_reserve(params)
    can_be_reserved = (self.minimum_count <= params[:no_of_pax] && self.maximum_count >= params[:no_of_pax])

    table_reservation = self.restaurant.reservations.where(reservation_date: params[:reservation_date], restaurant_table_id: self.id, reservation_time: params[:reservation_time]).first
    return {guest_count_ok: can_be_reserved,  table_reserved_ok: table_reservation.nil?}
  end

  protected
  def ensure_maximum_count
    if minimum_count > maximum_count
      throw(:abort)
    end
  end
end
