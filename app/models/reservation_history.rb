class ReservationHistory < ApplicationRecord
  belongs_to :reservation
  belongs_to :restaurant_table
  belongs_to :restaurant_shift

  after_create_commit :send_emails

  def send_emails
    ReservationMailer.delay.guest_email(self)
    ReservationMailer.delay.restaurant_email(self)
  end
end
