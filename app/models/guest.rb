class Guest < ApplicationRecord
  include Formats::Guest
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :name, presence: true
end
