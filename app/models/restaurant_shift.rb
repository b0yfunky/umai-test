class RestaurantShift < ApplicationRecord
  belongs_to :restaurant
  before_validation :ensure_end_time

  scope :range, -> (reservation_time) { where("end_time::time >= ? and start_time::time <=?", reservation_time, reservation_time) }

  protected
  def ensure_end_time
    if start_time >= end_time
      throw(:abort)
    end
  end
end
