module Formats::Guest
  def auth_details(options = {})
    {
      name: self.name,
      email: self.email,
      token: self.token
    }
  end
end
