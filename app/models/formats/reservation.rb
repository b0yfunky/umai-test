module Formats::Reservation
  def reservation_details(options = {})
    {
      reservation_number: self.restaurant.code + '-' + self.id.to_s,
      guest: self.guest.name
    }
  end

  def reservation_list_json(options = {})
    {
      reservation_number: self.restaurant.code + '-' + self.id.to_s,
      guest: self.guest.name,
      no_of_pax: self.no_of_pax,
      table: self.restaurant_table.code,
      time: self.reservation_time.strftime("%I:%M %p"),
      reservation_date: self.reservation_date
    }
  end
end
