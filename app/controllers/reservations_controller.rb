require "json-schema"
class ReservationsController < ApplicationController
  def index
    restaurant = Restaurant.find_by(code: params[:restaurant_code])
    if restaurant.present?
      render json: restaurant.reservations.map{|t| t.reservation_list_json }
    else
      raise NotFoundException.new("Restaurant does not exist", [params[:restaurant_code]])
    end
  end

  def create
    verify_schema
    restaurant = Restaurant.find_by(code: params[:restaurant_code])
    complete_reservation(restaurant, params, true)
  end

  def update
    verify_schema
    reservation = Reservation.find(params[:id])
    restaurant = reservation.restaurant
    complete_reservation(restaurant, params, false)
  end

  def complete_reservation(restaurant, params, new_reservation)
    restaurant_time_shift = restaurant.restaurant_shifts.range(params[:reservation_time])
    restaurant_table = restaurant.restaurant_tables.find_by(code: params[:restaurant_table_code])
    can_be_reserved = restaurant_table.can_reserve(params)

    if can_be_reserved[:guest_count_ok] && can_be_reserved[:table_reserved_ok]
      if restaurant_time_shift.present? && restaurant_table.present?

        if new_reservation
          reservation_params = {
            restaurant: restaurant,
            restaurant_table: restaurant_table,
            restaurant_shift: restaurant_time_shift.first,
            no_of_pax: params[:no_of_pax],
            reservation_time: params[:reservation_time],
            reservation_date: params[:reservation_date],
            guest_id: @user.id
          }
          reservation = Reservation.new(reservation_params)
          Reservation.transaction do
            if reservation.save!
              render json: {result: 'success', data: reservation.reservation_details}
            else
              raise BadRequestException.new("Bad Request", [exception.message])
            end
          end
        else
          update_params = {
            restaurant_table: restaurant_table,
            restaurant_shift: restaurant_time_shift.first,
            no_of_pax: params[:no_of_pax],
            reservation_time: params[:reservation_time],
            reservation_date: params[:reservation_date]
          }
          reservation = Reservation.find(params[:id])
          if reservation.update!(update_params)
            render json: {result: 'success', data: reservation.reservation_details}
          else
            raise BadRequestException.new("Bad Request", [exception.message])
          end
        end
      else
        raise NotFoundException.new("Reservation Time does not match restaurant schedule", [params[:reservation_time]])
      end
    else
      if !can_be_reserved[:guest_count_ok]
        raise BadRequestException.new("Guest Count is more than the maximum capacity for the table selected", [{selected: params[:no_of_pax], maximum: restaurant_table.maximum_count}])
      elsif !can_be_reserved[:table_reserved_ok]
        raise BadRequestException.new("A reservation for with the same details exist", [request.request_parameters.except(:reservation)])
      end
    end
  end

  def get_schema
    reservation_schema = {
      "type" => "object",
      "required" => [
        "restaurant_code",
        "restaurant_table_code",
        "reservation_time",
        "reservation_date",
        "no_of_pax"
      ],
      "properties" => {
        "restaurant_code" => {
          "type" => "string",
          "enum" => Restaurant.all.pluck(:code)
        },
        "restaurant_table_code" => {
          "type" => "string",
          "enum" => RestaurantTable.all.pluck(:code)
        },
        "reservation_time" => {
          "type" => "string",
          "default" => ""
        },
        "reservation_date" => {
          "type" => "string",
          "format" => "date"
        },
        "no_of_pax" => {
          "type" => "integer",
          "minimum" => 1
        }
      }
    }

    return reservation_schema
  end


  def verify_schema
    schema = get_schema
    validate_params = request.request_parameters.except(:reservation)

    validation_errors = JSON::Validator.fully_validate(schema, validate_params)

    raise BadRequestException.new("Bad Request", validation_errors) if validation_errors.size > 0
  end
end
