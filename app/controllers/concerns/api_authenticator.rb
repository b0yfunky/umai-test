module ApiAuthenticator
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_api

    def authenticate_api
      if request.format.json?
        decoded_token = JsonWebToken.decode(http_auth_header) rescue {}
        if decoded_token.present? && (decoded_token["exp"].to_i > Time.now.to_i)
          @user = Guest.find(decoded_token["user_id"])
          Devise.secure_compare(@user.token, http_auth_header)
          if @user && Devise.secure_compare(@user.token, http_auth_header)
            sign_in @user, store: false
          else
            render json: { error: 'You need to sign in or sign up before continuing.' }, status: 401
          end
        else
          render json: { error: 'Token Expired' }, status: 422
        end
      elsif request.format.any?
        true
      end
    end
  end

  def http_auth_header
    if request.headers['Authorization'].present?
      return request.headers['Authorization'].split(' ').last
    end
  end
end
