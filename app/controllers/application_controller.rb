class HttpApiException < Exception
  def initialize(http_code, message, errors = [])
    @code = http_code
    @message = message
    @errors = errors
  end

  def code
    @code
  end

  def message
    @message
  end

  def errors
    @errors
  end

  def json_response
    response = {
      message: @message
    }
    response[:errors] = @errors if @errors
    response.to_json
  end
end

class BadRequestException < HttpApiException
  def initialize(message="Bad Request", errors = nil)
    super(400, message, errors)
  end
end

class NotFoundException < HttpApiException
  def initialize(message="Not Found", errors = nil)
    super(404, message, errors)
  end
end

class InternalException < HttpApiException
  def initialize(message="Internal Server Error", errors = nil)
    super(500, message, errors)
  end
end

class ApplicationController < ActionController::API
  rescue_from Exception, with: :generic_exception_handler
  rescue_from ActiveRecord::RecordNotFound, with: :not_found_response
  rescue_from NameError, with: :generic_exception_handler
  rescue_from ActionController::RoutingError, with: :generic_exception_handler
  rescue_from ActiveRecord::RecordInvalid, with: :error_response
  rescue_from HttpApiException, with: :error_response

  include ApiAuthenticator

  before_action :configure_permitted_parameters, if: :devise_controller?

  def error_response(exception)
    json_output = exception.json_response
    render status: exception.code, json: json_output
  end

  def generic_exception_handler(exception)
    internal_server_error = InternalException.new("Internal Server Error", [exception.message])
    error_response(internal_server_error)
  end

  def not_found_response(exception)
    not_found = NotFoundException.new("Not Found", [exception.message])
    error_response(not_found)
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
  end
end
