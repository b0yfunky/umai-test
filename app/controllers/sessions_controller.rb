class SessionsController <  Devise::SessionsController
  skip_before_action :authenticate_api
  def create
    self.resource = warden.authenticate!(scope: :guest)
    resource.update_attribute(:token, ::JsonWebToken.encode(user_id: resource.id))
    sign_in resource, store: false
    render json: {user: resource.auth_details}, status: 200
  end
end
