Rails.application.routes.draw do

  concern :api_routes do
    resources :reservations, only: [:index, :update, :new, :create]
  end

  scope :api, defaults: { format: :json } do
    devise_for :admins
    devise_for :guests, controllers: { sessions: :sessions, registrations: :registrations }
    concerns :api_routes
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
