require "rails_helper"

describe ReservationsController, type: :controller do
  let(:encoded_token) { token_generator(guest.id)}
  let(:guest) { create(:guest) }
  describe "POST #create" do
    let(:restaurant) { create(:restaurant) }
    let(:restaurant_table) { create(:restaurant_table, restaurant: restaurant) }
    let(:restaurant_shift) { create(:restaurant_shift, restaurant: restaurant) }
    let(:create_request) { {restaurant_code: restaurant.code, restaurant_table_code: restaurant_table.code, reservation_time: "11:00 AM", reservation_date: '2019-01-21', no_of_pax: 4 } }
    context "guest should be logged in" do
      before do
        guest.token = encoded_token
        guest.save
      end

      it "creates a reservation" do
        request.headers['Authorization'] = guest.token
        expected_response = {
          result: 'success',
          data: {
            reservation_number: restaurant.code + '-1' ,
            guest: guest.name
          }
        }

        post :create, params: create_request
        expect(json).to eq expected_response
        expect(response.status).to eq 200
      end
    end
  end
end
