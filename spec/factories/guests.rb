FactoryBot.define do
  factory :guest do
    email { Faker::Internet.email }
    name { 'guest 1' }
    password { 'changeme!' }
    password_confirmation { 'changeme!' }
  end
end
