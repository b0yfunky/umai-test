FactoryBot.define do
  factory :restaurant_shift do
    association :restaurant
    name { 'Morning Shift' }
    start_time { "09:00 AM" }
    end_time { '01:00 PM' }
  end
end
