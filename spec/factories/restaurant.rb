FactoryBot.define do
  factory :restaurant do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    phone { '0123456789' }
    code { 'R1' }
  end
end
