FactoryBot.define do
  factory :restaurant_table do
    association :restaurant
    name { 'Table 1' }
    code { "T1" }
    minimum_count { 1 }
    maximum_count { 6 }
  end
end
