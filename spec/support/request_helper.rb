module Requests
  module JsonHelpers
    def json
      JSON.parse(response.body)
    end
  end

  module HTTPHelpers
    def get_url(url, params)
      headers = { "ACCEPT" => "application/json"}
      get(url, params, headers)
    end

    def post_url(url, params)
      headers = { "ACCEPT" => "application/json"}
      post(url, params, headers)
    end
  end
end
