require 'rails_helper'

RSpec.describe Restaurant, type: :model do
  subject {described_class.new}
  describe "Validations" do
    let(:valid_attributes) {{email: 'restaurant@email.com', name: 'Restaurant 1', code: 'RI', phone: '0123456789'}}

    it "is valid with valid attributes" do
      subject = described_class.new(valid_attributes)
      expect(subject).to be_valid
    end

    it "is not valid with invalid attributes" do
      expect(subject).to_not be_valid
    end
  end
end
