require 'rails_helper'

RSpec.describe Guest, type: :model do
  subject {described_class.new}
  describe "Validations" do
    let(:valid_attributes) {{email: 'test@test.com', name: 'guest 1', password: 'password', password_confirmation: 'password'}}
    let(:invalid_attributes) {{email: 'ab.com', name: '', password: 'password', password_confirmation: 'password'}}

    it "is valid with valid attributes" do
      subject = described_class.new(valid_attributes)
      expect(subject).to be_valid
    end

    it "is not valid with wrong email" do
      subject.email = "ab.com"
      expect(subject).to_not be_valid
    end
  end
end
