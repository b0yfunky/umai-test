class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.integer :guest_id
      t.integer :restaurant_id
      t.integer :restaurant_table_id
      t.integer :restaurant_shift_id
      t.integer :no_of_pax
      t.time :reservation_time
      t.date :reservation_date
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
  end
end
