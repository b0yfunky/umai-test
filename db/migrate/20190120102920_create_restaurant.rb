class CreateRestaurant < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :email, null: false, default: ""
      t.string :phone
      t.string :code
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
    add_index :restaurants, :email, unique: true
  end
end
