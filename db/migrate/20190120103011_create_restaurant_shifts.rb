class CreateRestaurantShifts < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurant_shifts do |t|
      t.integer :restaurant_id
      t.string :name
      t.time :start_time
      t.time :end_time
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
  end
end
