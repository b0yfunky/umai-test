class CreateRestaurantTables < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurant_tables do |t|
      t.integer :restaurant_id
      t.string :name
      t.string :code
      t.integer :minimum_count
      t.integer :maximum_count
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
  end
end
