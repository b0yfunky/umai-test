# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Guest.create!(email: 'guest@email.com', password: 'password', password_confirmation: 'password', name: 'Guest1')
restaurant = Restaurant.create!(name: 'Restaurant 1', email: 'restaurant@email.com', phone: '0123456789', code: 'R1')
Admin.create!(email: 'admin@email.com', password: 'password', password_confirmation: 'password', name: 'Admin Restaurant 1', restaurant: restaurant)
RestaurantTable.create!(restaurant: restaurant, name: 'Table 1', minimum_count: 1, maximum_count: 4, code: 'T1')
RestaurantTable.create!(restaurant: restaurant, name: 'Table 2', minimum_count: 1, maximum_count: 6, code: 'T2')
RestaurantShift.create!(restaurant: restaurant, name: 'Morning Shift', start_time: "09:00 AM", end_time: "01:00 PM"  )
RestaurantShift.create!(restaurant: restaurant, name: 'Afternoon Shift', start_time: "01:01 PM", end_time: "06:00 PM"  )
RestaurantShift.create!(restaurant: restaurant, name: 'Evening Shift', start_time: "06:01 PM", end_time: "11:00 PM"  )
